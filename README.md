# MakeLabReport

Automatically create a professional lab report without the repetitive tasks or the bloat of a typical word processor.

## Problem

I needed to make a professional looking lab report with screenshots of my source code to submit to a professor. Pasting the screenshots into LibreOffice is a repetitive and laggy process.

## Requirements

Note: Commands starting with '#' should be run as root or using 'sudo'.

- img2pdf
- poppler-utils
- groff

**Debian/Ubuntu Quick Install**

`# apt install img2pdf poppler-utils groff`

## Setup

- Clone the repo: `$ git clone https://gitlab.com/nathanharmon/makelabreport.git`
- Create a file calle "author.txt" under the grandparent folder with just your name in it. Do this for each course.
- Run `# ./create_symlink.sh`

## Directory Layout

The great grandparent folder of the image files should be the course name.

The parent folder should be the lab's specific name.

Ex. CS/Labs/Lab 1/[screenshots here]

## Use

- Navigate to the lab's folder (with the .png screenshots)
- Run `$ makelabreport`


## License

Copyright 2021 Nathan Harmon

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
