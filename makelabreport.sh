#!/bin/sh

<<COMMENT

makelabreport/makelabreport.sh - Create a lab report with a cover page and the screenshots of the source code.

Copyright 2021 Nathan Harmon

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

COMMENT

# The location of this file
loc="$(dirname $(readlink -f $(which makelabreport)))"

# The title of the parent folder should be the lab name.
lab="$(basename "$PWD")"

# The title of the grandparent folder should be the course name.
course="$(readlink -f ../.. | xargs basename)"

title="$course: $lab"
author="$(cat $(readlink -f ../..)/author.txt)"
date="$(date +'%d %B %Y')"

sed "s/<title>/$title/; s/<author>/$author/; s/<date>/$date/" $loc/theme.ms | groff -Tpdf -ms > cover.pdf

img2pdf *.png -o pages.pdf

pdfunite cover.pdf pages.pdf "Lab Report.pdf"

# clean up output files
rm cover.pdf pages.pdf

echo "Your lab report has been created. It is called 'Lab Report.pdf'\nMakeLabReport by Nathan Harmon"
