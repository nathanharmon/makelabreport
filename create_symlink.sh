#!/bin/sh

<<COMMENT

makelabreport/create_symlink.sh - Create the symbolic link to use makelabreport anywhere.

Copyright 2021 Nathan Harmon

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

COMMENT

# The location of this file
# Because the full path is required, a more extensive loc variable is used.
loc=$(
  cd $(dirname "$0")
  pwd
)

ln -s $loc/makelabreport.sh /usr/bin/makelabreport &&
echo "Symlink created. You can now run makelabreport!\nMakeLabReport by Nathan Harmon"
